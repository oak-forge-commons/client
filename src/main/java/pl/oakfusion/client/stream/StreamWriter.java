package pl.oakfusion.client.stream;

import pl.oakfusion.client.Writer;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class StreamWriter implements Writer {

	private final BufferedWriter writer;

	public StreamWriter(OutputStream stream) {
		writer = new BufferedWriter(new OutputStreamWriter(stream));
	}

	@Override
	public void writeLine(String string) {
		try {
			writer.write(string);
			writer.newLine();
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void write(String string) {
		try {
			writer.write(string);
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
