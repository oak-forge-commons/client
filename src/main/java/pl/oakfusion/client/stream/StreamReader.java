package pl.oakfusion.client.stream;

import pl.oakfusion.client.Reader;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class StreamReader implements Reader {

    private final BufferedReader reader;

	public StreamReader(InputStream stream) {
		reader = new BufferedReader(new InputStreamReader(stream));
	}

	public List<String> read() throws IOException {
		return Arrays.stream(reader.readLine()
				.trim()
				.toUpperCase()
				.split("\\s+"))
				.collect(Collectors.toList());
	}
}
