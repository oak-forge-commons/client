package pl.oakfusion.client.stream;

@FunctionalInterface
public interface ViewRenderer {
    void render(String text);
}
