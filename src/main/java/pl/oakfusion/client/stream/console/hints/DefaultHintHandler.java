package pl.oakfusion.client.stream.console.hints;

import pl.oakfusion.client.data.*;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.valueOf;

public class DefaultHintHandler implements HintHandler {

    private final String NUMERIC_REGEX = "[-]?[0-9]*(\\.[0-9])*";
    private final Map<String, String> commandsMap;
    private final Map<String, Tree<String>> optionTreesMap;
    private final HintTranslationConfigurationMapping hintTranslationConfigurationMapping;
    private final StringBuilder stringBuilder = new StringBuilder();

    public DefaultHintHandler(Map<String, String> commandsMap, Map<String, Tree<String>> optionTreesMap) throws IOException {
        this.commandsMap = commandsMap;
        this.optionTreesMap = optionTreesMap;
        hintTranslationConfigurationMapping = new HintTranslationConfigurationMapping();
        hintTranslationConfigurationMapping.parseTranslations();
    }

    @Override
    public String handle(UserInput userInput) {
        stringBuilder.setLength(0);
        if (commandsMap.containsKey(userInput.getMnemonic())) {
            return handleInvalidOptions(userInput);
        } else {
            return handleInvalidMnemonic(userInput.getMnemonic());
        }
    }

    private String handleInvalidMnemonic(String mnemonic) {
        char firstLetter = mnemonic.charAt(0);
        stringBuilder.append("Command incorrect. Did you mean one of the following?\n");
        optionTreesMap.forEach((key, value) -> {
            if (key.startsWith(valueOf(firstLetter))) {
                stringBuilder.append(value.getRoot().getData()).append("\n");
            }
        });
        return stringBuilder.toString();
    }

    private String handleInvalidOptions(UserInput userInput) {
        List<String> paths = new ArrayList<>();
        List<String> userInputOptions = userInput.getCommandOptions();
        Tree<String> messageTree = optionTreesMap.get(userInput.getMnemonic());
        Tree.Node<String> lastValidOption = getLastValidOption(userInputOptions, messageTree);
        if (userInputOptions.isEmpty()) {
            paths = getPaths(lastValidOption, "", paths);
        } else {
            paths = getPaths(lastValidOption, "", paths)
                    .stream()
                    .map(path -> userInput.getMnemonic() + " " + String.join(" ", userInputOptions.subList(0, userInputOptions.size() - 1)) + " " + path)
                    .collect(Collectors.toList());
        }

        return getPathsHints(paths);
    }

    private List<String> getPaths(Tree.Node<String> treeNode, String path, List<String> accumulator) {
        if (treeNode != null) {
            if (hintTranslationConfigurationMapping.getHintTranslation().containsKey(treeNode.getData())) {
                path += hintTranslationConfigurationMapping.getHintTranslation().get(treeNode.getData());
            } else {
                path += treeNode.getData() + " ";
            }

            for (int i = 0; i < treeNode.getChildren().size(); i++) {
                getPaths(treeNode.getChildren().get(i), path, accumulator);
            }
            if (treeNode.getChildren().size() == 0) accumulator.add(path.trim());
        }
        return accumulator;
    }

    private String getPathsHints(List<String> paths) {
        stringBuilder.append("Command incorrect. You can try one of the following combinations:\n");
        for (String path : paths) {
            stringBuilder.append(path).append("\n");
        }
        return stringBuilder.toString();
    }

    private Tree.Node<String> getLastValidOption(List<String> userInputOptions, Tree<String> commandTree) {
        Tree.Node<String> lastValidOption = commandTree.getRoot();
        List<String> numericOptions = new ArrayList<>();
        Tree.Node<String> optionNode;
        boolean isPreviousNumeric = false;

        for (int i = 0; i < userInputOptions.size(); i++) {
            while (i < userInputOptions.size() && userInputOptions.get(i).matches(NUMERIC_REGEX)) {
                numericOptions.add(userInputOptions.get(i));
                i++;
                isPreviousNumeric = true;
            }

            if (isPreviousNumeric) {
                optionNode = lastValidOption.getChild(valueOf(numericOptions.size()));
                if (optionNode == null) {
                    optionNode = lastValidOption.getChild("$");
                }
                isPreviousNumeric = false;
            } else {
                optionNode = lastValidOption.getChild(userInputOptions.get(i));
            }

            if (optionNode == null) {
                return lastValidOption;
            } else {
                lastValidOption = optionNode;
            }
        }
        return lastValidOption;
    }
}
