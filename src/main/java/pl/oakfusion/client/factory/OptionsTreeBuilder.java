package pl.oakfusion.client.factory;

import lombok.NoArgsConstructor;
import pl.oakfusion.client.data.Tree;

import java.util.List;

@NoArgsConstructor
public class OptionsTreeBuilder {

	public Tree<String> buildTree(String mnemonic, List<List<String>> paths) {
		Tree<String> optionsTree = new Tree<>(mnemonic);
		Tree.Node<String> currentPos;

		if (paths == null) {
			return optionsTree;
		}

		for (List<String> path : paths) {

			currentPos = optionsTree.getRoot();

			for (String option : path) {
				currentPos = optionsTree.addChild(option, currentPos);
			}
		}

		return optionsTree;
	}
}
