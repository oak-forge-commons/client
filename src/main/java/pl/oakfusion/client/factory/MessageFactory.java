package pl.oakfusion.client.factory;

import pl.oakfusion.client.data.UserInput;
import pl.oakfusion.data.message.Message;

@FunctionalInterface
public interface MessageFactory<M extends Message> {

    M createMessage(UserInput input);
}

