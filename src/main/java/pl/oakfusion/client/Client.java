package pl.oakfusion.client;

import lombok.*;
import pl.oakfusion.bus.*;
import pl.oakfusion.client.data.UserInput;
import pl.oakfusion.client.factory.MessageFactory;
import pl.oakfusion.client.parser.UserInputParser;
import pl.oakfusion.client.view.*;
import pl.oakfusion.data.message.*;

public abstract class Client extends MessageDriven {

    protected final Reader reader;
    protected final Writer writer;

    @Getter
    protected final MessageFactory<Command> messageFactory;
    protected final MessageBus messageBus;
    @Getter
    @Setter
    protected UserInputParser userInputParser;

    protected Client(Reader reader, Writer writer, MessageFactory<Command> messageFactory, MessageBus messageBus, ViewRendererMapping viewRendererMapping, UserInputParser userInputParser) {
        super(messageBus);
        this.reader = reader;
        this.writer = writer;
        this.messageFactory = messageFactory;
        this.messageBus = messageBus;
        this.userInputParser = userInputParser;

        viewRendererMapping.registerViewHandlers();
    }

    public void handleMessageFromUser() {
        Message message = getCommandFromUser();
        post((Command) message);
    }

    private Message getCommandFromUser() {
        UserInput input = readUserInput();
        return getMessageFactory().createMessage(input);
    }


    protected abstract UserInput readUserInput();
}
