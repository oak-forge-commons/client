package pl.oakfusion.client.parser;

import pl.oakfusion.client.data.*;

import java.util.*;

import static java.lang.String.valueOf;

class TorpedoHandlers {

    private final String FLOAT_REGEX = "[-]?[0-9]*(\\.[0-9])*";
    private final String TARGETS_HANDLER_NAME = "TARGETS";
	private final Map<String, CommandParserHandler> commandHandlers;

	TorpedoHandlers(Map<String, CommandParserHandler> commandHandlers) {
		this.commandHandlers = commandHandlers;
	}

	void handle(Iterator<String> iterator, CommandParserHandlerContext context) {
		if (!iterator.hasNext()) {
			throw new UserInputException();
		}
		String next = iterator.next();
		Tree.Node<String> nextNode = context.getCurrentNode().getChild("N"+next);
		if (nextNode == null) {
			throw new UserInputException();
		}
        context.getUserInput().getCommandOptions().add("N" + next);
		commandHandlers.get(context.getCurrentNode().getData() + "_" + TARGETS_HANDLER_NAME).handle(iterator, new CommandParserHandlerContext(nextNode, context.getUserInput()));
	}

	void handleTorpedoTargets(Iterator<String> iterator, CommandParserHandlerContext context) {
        if (!iterator.hasNext()) {
            throw new UserInputException();
        }
        String next;
        List<String> targets = new ArrayList<>();
        while (iterator.hasNext()) {
            next = iterator.next();
            if (next.matches(FLOAT_REGEX)) {
                targets.add(next);
            } else {
                throw new UserInputException();
            }
        }

        Tree.Node<String> nextNode = context.getCurrentNode().getChild(valueOf(targets.size()));
        if (nextNode == null) {
            throw new UserInputException();
        }

        context.getUserInput().getCommandOptions().addAll(targets);
    }
}
