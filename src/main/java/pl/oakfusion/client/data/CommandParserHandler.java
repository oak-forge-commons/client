package pl.oakfusion.client.data;

import java.util.Iterator;

@FunctionalInterface
public interface CommandParserHandler {

    void handle(Iterator<String> iterator, CommandParserHandlerContext context);
}
