package pl.oakfusion.client.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.Getter;

import java.io.*;
import java.util.Map;

public class HintTranslationConfigurationMapping {

    private final InputStream inputStream;
    private final ObjectMapper objectMapper;
    @Getter
    Map<String, String> hintTranslation;

    public HintTranslationConfigurationMapping() {
        inputStream = getClass().getClassLoader().getResourceAsStream("hintTranslation.yml");
        objectMapper = new ObjectMapper(new YAMLFactory());
        objectMapper.addMixIn(HintTranslationConfigurationMapping.class, MessageConfigurationMappingMixIn.class);
    }

    public void parseTranslations() throws IOException {
        hintTranslation = objectMapper.readValue(inputStream, HintTranslationConfigurationMapping.class).getHintTranslation();
    }
}
