package pl.oakfusion.client.data;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public abstract class MessageConfigurationMixIn {
    @JsonProperty
    String name;
    @JsonProperty
    String mnemonic;
    @JsonProperty
    String shortestForm;
    @JsonProperty
    List<String> options;
}
