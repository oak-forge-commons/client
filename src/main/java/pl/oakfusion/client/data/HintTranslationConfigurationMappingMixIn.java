package pl.oakfusion.client.data;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.util.Map;

public abstract class HintTranslationConfigurationMappingMixIn {

    @JsonIgnore
    File file;
    @JsonIgnore
    ObjectMapper objectMapper;
    @JsonProperty
    Map<String, String> hintTranslations;
}
