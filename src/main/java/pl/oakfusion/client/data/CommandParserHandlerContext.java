package pl.oakfusion.client.data;

import lombok.*;

import java.util.*;

@Getter
@Setter
@AllArgsConstructor
public class CommandParserHandlerContext {

    private final Tree.Node<String> currentNode;
	private final UserInput userInput;
	private final List<String> optionParameters;

	public CommandParserHandlerContext(Tree.Node<String> currentNode, UserInput userInput) {
		this.currentNode = currentNode;
		this.userInput = userInput;
		optionParameters = new ArrayList<>();
	}
}
