package pl.oakfusion.client.data;

import lombok.*;

import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
public class UserInput {

    private String mnemonic;
	private List<String> commandOptions;

	public UserInput() {
		this.commandOptions = new ArrayList<>();
	}

	public List<String> toList() {
		List<String> userInput = new ArrayList<>();
		userInput.add(mnemonic);
		userInput.addAll(commandOptions);
		return userInput;
	}

	@Override
	public String toString() {
		return getMnemonic() + " " + getCommandOptions().stream().collect(Collectors.joining(" "));
	}
}
