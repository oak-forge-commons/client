package pl.oakfusion.client.data;

public class UserInputException extends RuntimeException {

    public UserInputException() {
    }

    public UserInputException(String message) {
        super(message);
    }
}
