package pl.oakfusion.client.data;

import lombok.*;

import java.util.*;

public class Tree<T> {

    @Getter
	@Setter
	private Node<T> root;

	public Tree(T rootData) {
		root = new Node<T>(rootData);
		root.data = rootData;
		root.parent = null;
		root.children = new ArrayList<>();
	}

	public Node<T> addChild(T child, Node<T> parent) {
		Node<T> nodeChild = new Node<T>(child);

		if (parent.getChildren().contains(nodeChild)) {
			return parent.getChildren().stream().filter(c -> c.equals(nodeChild)).findFirst().orElse(null);
		} else {
			nodeChild.setParent(parent);
			parent.getChildren().add(nodeChild);
			return nodeChild;
		}
	}

	@Getter
	@Setter
	public static class Node<T> {

        private T data;
		private Node<T> parent;
		private List<Node<T>> children;

		public Node(T data) {
			children = new LinkedList<Node<T>>();
			this.data = data;
		}

		public Node<T> getNode(T nodeData) {
			if (this.data == nodeData) return this;
			Node<T> childToFind = this.getChild(nodeData);
			if (childToFind != null) return childToFind;
			for (int i = 0; i < this.children.size(); i++) {
				if (this.children.get(i).getNode(nodeData) != null) return this.children.get(i);
			}
			return null;
		}

		public Node<T> getChild(T childData) {
			Node<T> childToFind = new Node<>(childData);

			return this.getChildren()
					.stream()
					.filter(child -> child.equals(childToFind))
					.findFirst()
					.orElse(null);
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			Node<?> node = (Node<?>) o;
			return Objects.equals(data, node.data);
		}

		@Override
		public int hashCode() {
			return data.hashCode();
		}
	}

}
