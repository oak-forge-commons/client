package pl.oakfusion.client.parser


import spock.lang.Specification

class UserInputParserSpockTest extends Specification {

    def userInputParser

    def setup() {
        userInputParser = new UserInputParser()
    }

    def "should correctly parse incorrect mnemonics"() {
        when:
        def parsingResult = userInputParser.parseInput(input)

        then:
        parsingResult.getUserInput().getMnemonic() == expectedMnemonic

        where:
        input      | expectedMnemonic
        ["SHELDS"] | "SHELDS"
        ["123a"]   | "123A"
        ["MaVe"]   | "MAVE"
    }

    def "should parse srscan command"() {
        when:
        def mnemonic = "SRSCAN"
        def parsingResult = userInputParser.parseInput(input)

        then:
        parsingResult.getUserInput().getMnemonic() == mnemonic
        parsingResult.getUserInput().getCommandOptions() == expectedOptions

        where:
        input                    | expectedOptions
        ["s"]                    | []
        ["s", "n"]               | ["NO"]
        ["sr", "no", "c"]        | ["NO", "CHART"]
        ["sr", "ch"]             | ["CHART"]
        ["sr", "ch", "no"]       | ["CHART", "NO"]
        ["sr", "a"]              | []
        ["sr", "ch", "no", "gg"] | ["CHART", "NO"]
        ["sr", "ch", "kg"]       | ["CHART"]
        ["sr", "no", "12"]       | ["NO"]
    }

    def "should parse move command"() {
        when:
        def mnemonic = "MOVE"
        def parsingResult = userInputParser.parseInput(input)

        then:
        parsingResult.getUserInput().getMnemonic() == mnemonic
        parsingResult.getUserInput().getCommandOptions() == expectedOptions

        where:
        input                                   | expectedOptions
        ["m", "a", "2", "5"]                    | ["AUTOMATIC", "2", "5"]
        ["m", "a", "2", "5", "4", "5"]          | ["AUTOMATIC", "2", "5", "4", "5"]
        ["m", "a", "2", "3", "force"]           | ["AUTOMATIC", "2", "3", "FORCE"]
        ["m", "ma", "2.5"]                      | ["MANUAL", "2.5"]
        ["m", "m", "2.5", "5.7"]                | ["MANUAL", "2.5", "5.7"]
        ["m", "au", "22.5", "5.7"]              | ["AUTOMATIC"]
        ["m", "aut", "-22", "2"]                | ["AUTOMATIC", "-22", "2"]
        ["m", "m", "22.5", "5.7"]               | ["MANUAL", "22.5", "5.7"]
        ["m", "m", "1", "f"]                    | ["MANUAL", "1", "FORCE"]
        ["m", "m", "22", "5.7"]                 | ["MANUAL", "22", "5.7"]
        ["m", "m", "22", "5.7", "FORCE"]        | ["MANUAL", "22", "5.7", "FORCE"]
        ["m", "m", "22", "2", "ABC"]            | ["MANUAL", "22", "2"]
        ["MoVe", "a", "2", "abc"]               | ["AUTOMATIC"]
        ["m", "a", "abc", "345.7"]              | ["AUTOMATIC"]
        ["m", "a", "2", "3", "4", "Force"]      | ["AUTOMATIC"]
        ["m", "a", "2", "3", "4", "Force", "5"] | ["AUTOMATIC"]
        ["m", "a", "2.5", "345.7", "2"]         | ["AUTOMATIC"]
        ["m", "a", "2", "5", "4", "5", "4"]     | ["AUTOMATIC"]
        ["m", "m", "2.5", "5.7", "9"]           | ["MANUAL"]
        ["m", "a", "f"]                         | ["AUTOMATIC"]
        ["m", "a", "f", "2", "5"]               | ["AUTOMATIC"]
        ["move", "2", "2.5", "345.7", "2"]      | []
        ["m", "m", "2", "abc"]                  | ["MANUAL", "2"]
        ["mo", "m", "abc", "345.7"]             | ["MANUAL"]
        ["mov", "m", "2.5", "345.7", "2"]       | ["MANUAL"]
        ["m", "f", "a", "2", "5"]               | []
        ["m", "f", "m", "2"]                    | []
        ["m", "2", "2.5", "345.7", "2"]         | []
        ["m", "j", "2.5", "345.7", "2"]         | []
    }

    def "should parse warp command"() {
        when:
        def mnemonic = "WARP"
        def parsingResult = userInputParser.parseInput(input)

        then:
        parsingResult.getUserInput().getMnemonic() == mnemonic
        parsingResult.getUserInput().getCommandOptions() == expectedOptions

        where:
        input             | expectedOptions
        ["w", "2.4"]      | ["2.4"]
        ["wa", "10"]      | ["10"]
        ["wa", "10", "5"] | []
        ["war", "12"]     | ["12"]
        ["warp", "0.1"]   | ["0.1"]
        ["w", "a"]        | []
    }

    def "should parse impulse command"() {
        when:
        def mnemonic = "IMPULSE"
        def parsingResult = userInputParser.parseInput(input)

        then:
        parsingResult.getUserInput().getMnemonic() == mnemonic
        parsingResult.getUserInput().getCommandOptions() == expectedOptions


        where:
        input                                 | expectedOptions
        ["i", "a", "2", "5"]                  | ["AUTOMATIC", "2", "5"]
        ["im", "a", "2", "5", "4", "5"]       | ["AUTOMATIC", "2", "5", "4", "5"]
        ["imp", "m", "2.5"]                   | ["MANUAL", "2.5"]
        ["im", "m", "2.5", "5.7"]             | ["MANUAL", "2.5", "5.7"]
        ["im", "a", "22.5", "5.7"]            | ["AUTOMATIC"]
        ["i", "m", "22.5", "5.7"]             | ["MANUAL", "22.5", "5.7"]
        ["i", "m", "22", "5.7"]               | ["MANUAL", "22", "5.7"]
        ["im", "a", "2", "abc"]               | ["AUTOMATIC"]
        ["imp", "a", "abc", "345.7"]          | ["AUTOMATIC"]
        ["impulse", "a", "2.5", "345.7", "2"] | ["AUTOMATIC"]
        ["im", "a", "2", "5", "4", "5", "4"]  | ["AUTOMATIC"]
        ["impul", "m", "2.5", "5.7", "9"]     | ["MANUAL"]
        ["im", "2", "2.5", "345.7", "2"]      | []
        ["i", "m", "2", "abc"]                | ["MANUAL"]
        ["i", "m", "abc", "345.7"]            | ["MANUAL"]
        ["i", "m", "2.5", "345.7", "2"]       | ["MANUAL"]
        ["i", "2", "2.5", "345.7", "2"]       | []
        ["i", "j", "2.5", "345.7", "2"]       | []
    }

    def "should parse shields command"() {
        when:
        def mnemonic = "SHIELDS"
        def parsingResult = userInputParser.parseInput(input)

        then:
        parsingResult.getUserInput().getMnemonic() == mnemonic
        parsingResult.getUserInput().getCommandOptions() == expectedOptions

        where:
        input                             | expectedOptions
        ["sh", "u"]                       | ["UP"]
        ["shie", "d", "2", "5", "4", "5"] | ["DOWN"]
        ["shields", "down", "2.5"]        | ["DOWN"]
        ["shields", "transfer", "-2.5"]   | ["TRANSFER", "-2.5"]
        ["sh", "t", "22.5"]               | ["TRANSFER", "22.5"]
        ["sh", "d", "2"]                  | ["DOWN"]
        ["sh", "u", "22"]                 | ["UP"]
        ["sh", "t", "abc"]                | ["TRANSFER"]
        ["sh", "t", "22", "2"]            | ["TRANSFER"]
        ["sh", "t", "2.5", "345.7", "2"]  | ["TRANSFER"]
        ["sh", "2", "2.5", "345.7", "2"]  | []
    }

    def "should parse phasers command"() {
        when:
        def mnemonic = "PHASERS"
        def parsingResult = userInputParser.parseInput(input)

        then:
        parsingResult.getUserInput().getMnemonic() == mnemonic
        parsingResult.getUserInput().getCommandOptions() == expectedOptions

        where:
        input                                    | expectedOptions
        ["p", "a", "2"]                          | ["AUTOMATIC", "2"]
        ["p", "2"]                               | ["AUTOMATIC", "2"]
        ["p", "m", "1"]                          | ["MANUAL", "1"]
        ["p", "m", "1", "2"]                     | ["MANUAL", "1", "2"]
        ["p", "m", "1", "2", "3"]                | ["MANUAL", "1", "2", "3"]
        ["p", "m", "1", "2", "3", "5", "6", "7"] | ["MANUAL", "1", "2", "3", "5", "6", "7"]
        ["p", "m"]                               | ["MANUAL"]
        ["p", "m", "1", "2.3"]                   | ["MANUAL"]
        ["p", "a"]                               | ["AUTOMATIC"]
        ["p", "1", "2", "3"]                     | ["AUTOMATIC"]
        ["p", "1.2", "2", "2.3"]                 | ["AUTOMATIC"]
        ["p", "-2"]                              | ["AUTOMATIC", "-2"]
        ["p", "abc", "2", "3"]                   | []
        ["p", "a", "abc", "3"]                   | ["AUTOMATIC"]
        ["p", "m", "2", "abc"]                   | ["MANUAL"]
        ["p", "m", "2", "no"]                    | ["MANUAL", "2", "NO"]
        ["p", "m", "2", "2", "no"]               | ["MANUAL", "2", "2", "NO"]
        ["p", "no", "m", "2", "2"]               | ["NO", "MANUAL", "2", "2"]
        ["p", "no", "m", "2", "2", "2", "no"]    | ["NO", "MANUAL"]
        ["p", "a", "2", "no"]                    | ["AUTOMATIC", "2", "NO"]
        ["p", "no", "a", "2"]                    | ["NO", "AUTOMATIC", "2"]
        ["p", "no", "a", "1", "no"]              | ["NO", "AUTOMATIC"]
        ["p", "no", "2"]                         | ["NO", "AUTOMATIC", "2"]
        ["p", "2", "no"]                         | ["AUTOMATIC", "2", "NO"]
        ["p", "no", "2", "no"]                   | ["NO", "AUTOMATIC"]
        ["p", "no", "ads"]                       | ["NO"]
        ["p", "no", "auto", "ad"]                | ["NO", "AUTOMATIC"]
        ["p", "no"]                              | ["NO"]
    }

    def "should parse cloak command"() {
        when:
        def mnemonic = "CLOAK"
        def parsingResult = userInputParser.parseInput(input)

        then:
        parsingResult.getUserInput().getMnemonic() == mnemonic
        parsingResult.getUserInput().getCommandOptions() == expectedOptions

        where:
        input                   | expectedOptions
        ["cloak", "on"]         | ["ON"]
        ["cloak", "off"]        | ["OFF"]
        ["cloak", "12"]         | []
        ["cloak", "o"]          | []
        ["cloak", "of"]         | []
        ["cloak", "ON", "12"]   | []
        ["cloak", "OFF", "abs"] | []
    }

    def "should parse torpedo command"() {
        when:
        def mnemonic = "TORPEDO"
        def parsingResult = userInputParser.parseInput(input)

        then:
        parsingResult.getUserInput().getMnemonic() == mnemonic
        parsingResult.getUserInput().getCommandOptions() == expectedOptions

        where:
        input                                          | expectedOptions
        ["to", "1", "5", "5"]                          | ["N1", "5", "5"]
        ["to", "2", "5", "5"]                          | ["N2", "5", "5"]
        ["to", "2", "5", "5", "6", "6"]                | ["N2", "5", "5", "6", "6"]
        ["to", "3", "5", "5"]                          | ["N3", "5", "5"]
        ["to", "3", "2.3", "6.7"]                      | ["N3", "2.3", "6.7"]
        ["to", "3", "5", "5", "6", "6", "7", "7"]      | ["N3", "5", "5", "6", "6", "7", "7"]
        ["to", "3", "5", "5", "6", "6"]                | ["N3"]
        ["to", "1", "5", "5", "6"]                     | ["N1"]
        ["to", "2", "5", "5", "6", "6", "7"]           | ["N2"]
        ["to", "3", "5", "5", "6", "6", "7", "7", "8"] | ["N3"]
        ["to", "3", "12", "10"]                        | ["N3", "12", "10"]
        ["to", "2", "abc", "2"]                        | ["N2"]
        ["to", "4"]                                    | []
        ["to", "2", "abc"]                             | ["N2"]
        ["to", "abc", "3", "4"]                        | []
    }

    def "should parse rest command"() {
        when:
        def mnemonic = "REST"
        def parsingResult = userInputParser.parseInput(input)

        then:
        parsingResult.getUserInput().getMnemonic() == mnemonic
        parsingResult.getUserInput().getCommandOptions() == expectedOptions

        where:
        input             | expectedOptions
        ["r", "1"]        | ["1"]
        ["r", "135"]      | ["135"]
        ["r", "-2"]       | ["-2"]
        ["r", "abc"]      | []
        ["r", "2", "3"]   | []
        ["r", "2", "abc"] | []
    }

    def "should parse freeze command"() {
        when:
        def mnemonic = "FREEZE"
        def parsingResult = userInputParser.parseInput(input)

        then:
        parsingResult.getUserInput().getMnemonic() == mnemonic
        parsingResult.getUserInput().getCommandOptions() == expectedOptions

        where:
        input                               | expectedOptions
        ["freeze", "file.trk"]              | ["FILE.TRK"]
        ["freeze", "file.TRK"]              | ["FILE.TRK"]
        ["freeze", "file"]                  | []
        ["freeze", "file.txt"]              | []
        ["freeze", "file.txt", "file2.txt"] | []
    }

    def "should parse request command"() {
        when:
        def mnemonic = "REQUEST"
        def parsingResult = userInputParser.parseInput(input)

        then:
        parsingResult.getUserInput().getMnemonic() == mnemonic
        parsingResult.getUserInput().getCommandOptions() == expectedOptions

        where:
        input         | expectedOptions
        ["req", "s"]  | ["SHIELDS"]
        ["req", "k"]  | ["KLINGONS"]
        ["req", "zd"] | []
        ["req", "t"]  | ["TORPEDOES"]
        ["req", "d"]  | ["DATE"]
        ["req", "ti"] | ["TIME"]
        ["req", "ba"] | ["BASES"]
        ["req", "b"]  | []
    }

    def "should parse probe command"() {
        when:
        def mnemonic = "PROBE"
        def parsingResult = userInputParser.parseInput(input)

        then:
        parsingResult.getUserInput().getMnemonic() == mnemonic
        parsingResult.getUserInput().getCommandOptions() == expectedOptions

        where:
        input                                  | expectedOptions
        ["pr", "a", "2", "5"]                  | ["AUTOMATIC", "2", "5"]
        ["pr", "a", "2", "5", "4", "5"]        | ["AUTOMATIC", "2", "5", "4", "5"]
        ["pr", "m", "2.5"]                     | ["MANUAL", "2.5"]
        ["pr", "m", "2.5", "5.7"]              | ["MANUAL", "2.5", "5.7"]
        ["pr", "ar", "m", "2.1", "2"]          | ["ARMED", "MANUAL", "2.1", "2"]
        ["prob", "arm", "au", "2", "10"]       | ["ARMED", "AUTOMATIC", "2", "10"]
        ["prob", "arm", "au", "2.3", "10"]     | ["ARMED", "AUTOMATIC"]
        ["prob", "arm", "aug"]                 | ["ARMED"]
        ["prob", "a", "a", "2", "3"]           | ["AUTOMATIC"]
        ["pr", "a", "22.5", "5.7"]             | ["AUTOMATIC"]
        ["pr", "m", "22.5", "5.7"]             | ["MANUAL", "22.5", "5.7"]
        ["pr", "m", "22", "5.7"]               | ["MANUAL", "22", "5.7"]
        ["pr", "a", "2", "abc"]                | ["AUTOMATIC"]
        ["pr", "a", "abc", "345.7"]            | ["AUTOMATIC"]
        ["pr", "a", "2.5", "345.7", "2"]       | ["AUTOMATIC"]
        ["prob", "a", "2", "5", "4", "5", "4"] | ["AUTOMATIC"]
        ["probe", "m", "2.5", "5.7", "9"]      | ["MANUAL"]
        ["pr", "2", "2.5", "345.7", "2"]       | []
        ["pr", "m", "2", "abc"]                | ["MANUAL"]
        ["pr", "m", "abc", "345.7"]            | ["MANUAL"]
        ["pr", "m", "2.5", "345.7", "2"]       | ["MANUAL"]
        ["pr", "2", "2.5", "345.7", "2"]       | []
        ["pr", "j", "2.5", "345.7", "2"]       | []
    }

    def "should parse help command"() {
        when:
        def mnemonic = "HELP"
        def parsingResult = userInputParser.parseInput(input)

        then:
        parsingResult.getUserInput().getMnemonic() == mnemonic
        parsingResult.getUserInput().getCommandOptions() == expectedOptions

        where:
        input                 | expectedOptions
        ["help", "s"]         | ["SRSCAN"]
        ["help", "srscan"]    | ["SRSCAN"]
        ["help", "st"]        | ["STATUS"]
        ["help", "status"]    | ["STATUS"]
        ["help", "l"]         | ["LRSCAN"]
        ["help", "lrscan"]    | ["LRSCAN"]
        ["help", "c"]         | ["CHART"]
        ["help", "chart"]     | ["CHART"]
        ["help", "da"]        | ["DAMAGES"]
        ["help", "damages"]   | ["DAMAGES"]
        ["help", "m"]         | ["MOVE"]
        ["help", "move"]      | ["MOVE"]
        ["help", "w"]         | ["WARP"]
        ["help", "warp"]      | ["WARP"]
        ["help", "i"]         | ["IMPULSE"]
        ["help", "impulse"]   | ["IMPULSE"]
        ["help", "sh"]        | ["SHIELDS"]
        ["help", "shields"]   | ["SHIELDS"]
        ["help", "p"]         | ["PHASERS"]
        ["help", "phasers"]   | ["PHASERS"]
        ["help", "cloak"]     | ["CLOAK"]
        ["help", "ca"]        | ["CAPTURE"]
        ["help", "capture"]   | ["CAPTURE"]
        ["help", "rep"]       | ["REPORT"]
        ["help", "report"]    | ["REPORT"]
        ["help", "co"]        | ["COMPUTER"]
        ["help", "computer"]  | ["COMPUTER"]
        ["help", "to"]        | ["TORPEDO"]
        ["help", "torpedo"]   | ["TORPEDO"]
        ["help", "d"]         | ["DOCK"]
        ["help", "dock"]      | ["DOCK"]
        ["help", "r"]         | ["REST"]
        ["help", "rest"]      | ["REST"]
        ["help", "mayday"]    | ["MAYDAY"]
        ["help", "abandon"]   | ["ABANDON"]
        ["help", "destruct"]  | ["DESTRUCT"]
        ["help", "quit"]      | ["QUIT"]
        ["help", "se"]        | ["SENSORS"]
        ["help", "sensors"]   | ["SENSORS"]
        ["help", "o"]         | ["ORBIT"]
        ["help", "orbit"]     | ["ORBIT"]
        ["help", "t"]         | ["TRANSPORT"]
        ["help", "transport"] | ["TRANSPORT"]
        ["help", "shu"]       | ["SHUTTLE"]
        ["help", "shuttle"]   | ["SHUTTLE"]
        ["help", "mi"]        | ["MINE"]
        ["help", "mine"]      | ["MINE"]
        ["help", "cr"]        | ["CRYSTALS"]
        ["help", "crystals"]  | ["CRYSTALS"]
        ["help", "pl"]        | ["PLANETS"]
        ["help", "planets"]   | ["PLANETS"]
        ["help", "planets"]   | ["PLANETS"]
        ["help", "freeze"]    | ["FREEZE"]
        ["help", "req"]       | ["REQUEST"]
        ["help", "deathray"]  | ["DEATHRAY"]
        ["help", "pr"]        | ["PROBE"]
        ["help", "e"]         | ["EMEXIT"]
        ["help", "command"]   | []
        ["help", "movr"]      | []
    }

    def "should parse select command"() {
        when:
        def mnemonic = "SELECT"
        def parsingResult = userInputParser.parseInput(input)

        then:
        parsingResult.getUserInput().getMnemonic() == mnemonic
        parsingResult.getUserInput().getCommandOptions() == expectedOptions

        where:
        input                           | expectedOptions
        ["sel", "2"]                    | ["2"]
        ["select", "Klingon1"]          | ["KLINGON1"]
        ["sel", "10", "20"]             | []
        ["sele", "1012312"]             | ["1012312"]
        ["sel", "12.2"]                 | []
        ["sel", "klingon1", "klingon2"] | []
    }
}
