package pl.oakfusion.client.factory

import spock.lang.Specification

class AbbreviationFactoryTest extends Specification {


    def "should correctly create abbreviations given just name"() {
        given:
        def abbreviationFactory = new AbbreviationFactory()

        when:
        def abbreviationsList = abbreviationFactory.getAbbreviations(fullName)

        then:
        abbreviationsList == expectedAbbreviations

        where:
        fullName    | expectedAbbreviations
        "TEST"      | ["T", "TE", "TES", "TEST"]
        "MOVE"      | ["M", "MO", "MOV", "MOVE"]
        "TORPEDO"   | ["T", "TO", "TOR", "TORP", "TORPE", "TORPED", "TORPEDO"]
        "SRSCAN"    | ["S", "SR", "SRS", "SRSC", "SRSCA", "SRSCAN"]
        "STATUS"    | ["S", "ST", "STA", "STAT", "STATU", "STATUS"]
        "LRSCAN"    | ["L", "LR", "LRS", "LRSC", "LRSCA", "LRSCAN"]
        "CHART"     | ["C", "CH", "CHA", "CHAR", "CHART"]
        "DAMAGES"   | ["D", "DA", "DAM", "DAMA", "DAMAG", "DAMAGE", "DAMAGES"]
        "WARP"      | ["W", "WA", "WAR", "WARP"]
        "IMPULSE"   | ["I", "IM", "IMP", "IMPU", "IMPUL", "IMPULS", "IMPULSE"]
        "SHIELDS"   | ["S", "SH", "SHI", "SHIE", "SHIEL", "SHIELD", "SHIELDS"]
        "PHASERS"   | ["P", "PH", "PHA", "PHAS", "PHASE", "PHASER", "PHASERS"]
        "CLOAK"     | ["C", "CL", "CLO", "CLOA", "CLOAK"]
        "CAPTURE"   | ["C", "CA", "CAP", "CAPT", "CAPTU", "CAPTUR", "CAPTURE"]
        "REPORT"    | ["R", "RE", "REP", "REPO", "REPOR", "REPORT"]
        "COMPUTER"  | ["C", "CO", "COM", "COMP", "COMPU", "COMPUT", "COMPUTE", "COMPUTER"]
        "DOCK"      | ["D", "DO", "DOC", "DOCK"]
        "REST"      | ["R", "RE", "RES", "REST"]
        "MAYDAY"    | ["M", "MA", "MAY", "MAYD", "MAYDA", "MAYDAY"]
        "ABANDON"   | ["A", "AB", "ABA", "ABAN", "ABAND", "ABANDO", "ABANDON"]
        "DESTRUCT"  | ["D", "DE", "DES", "DEST", "DESTR", "DESTRU", "DESTRUC", "DESTRUCT"]
        "QUIT"      | ["Q", "QU", "QUI", "QUIT"]
        "SENSORS"   | ["S", "SE", "SEN", "SENS", "SENSO", "SENSOR", "SENSORS"]
        "ORBIT"     | ["O", "OR", "ORB", "ORBI", "ORBIT"]
        "TRANSPORT" | ["T", "TR", "TRA", "TRAN", "TRANS", "TRANSP", "TRANSPO", "TRANSPOR", "TRANSPORT"]
        "SHUTTLE"   | ["S", "SH", "SHU", "SHUT", "SHUTT", "SHUTTL", "SHUTTLE"]
        "MINE"      | ["M", "MI", "MIN", "MINE"]
        "CRYSTALS"  | ["C", "CR", "CRY", "CRYS", "CRYST", "CRYSTA", "CRYSTAL", "CRYSTALS"]
        "PLANETS"   | ["P", "PL", "PLA", "PLAN", "PLANE", "PLANET", "PLANETS"]
        "FREEZE"    | ["F", "FR", "FRE", "FREE", "FREEZ", "FREEZE"]
        "REQUEST"   | ["R", "RE", "REQ", "REQU", "REQUE", "REQUES", "REQUEST"]
        "DEATHRAY"  | ["D", "DE", "DEA", "DEAT", "DEATH", "DEATHR", "DEATHRA", "DEATHRAY"]
        "PROBE"     | ["P", "PR", "PRO", "PROB", "PROBE"]
        "EMEXIT"    | ["E", "EM", "EME", "EMEX", "EMEXI", "EMEXIT"]
        "HELP"      | ["H", "HE", "HEL", "HELP"]
        "SOME"      | ["S", "SO", "SOM", "SOME"]
        "GLASSES"   | ["G", "GL", "GLA", "GLAS", "GLASS", "GLASSE", "GLASSES"]
    }

    def "should correctly create abbreviation given name and shortest form"() {
        given:
        def abbreviationFactory = new AbbreviationFactory()

        when:
        def abbreviationsList = abbreviationFactory.getAbbreviations(fullName, shortestForm)

        then:
        abbreviationsList == expectedAbbreviations

        where:
        fullName    | shortestForm | expectedAbbreviations
        "TEST"      | "TE"         | ["TE", "TES", "TEST"]
        "MOVE"      | "M"          | ["M", "MO", "MOV", "MOVE"]
        "TORPEDO"   | "TO"         | ["TO", "TOR", "TORP", "TORPE", "TORPED", "TORPEDO"]
        "SRSCAN"    | "S"          | ["S", "SR", "SRS", "SRSC", "SRSCA", "SRSCAN"]
        "STATUS"    | "ST"         | ["ST", "STA", "STAT", "STATU", "STATUS"]
        "LRSCAN"    | "L"          | ["L", "LR", "LRS", "LRSC", "LRSCA", "LRSCAN"]
        "CHART"     | "C"          | ["C", "CH", "CHA", "CHAR", "CHART"]
        "DAMAGES"   | "DA"         | ["DA", "DAM", "DAMA", "DAMAG", "DAMAGE", "DAMAGES"]
        "WARP"      | "WARP"       | ["WARP"]
        "IMPULSE"   | "I"          | ["I", "IM", "IMP", "IMPU", "IMPUL", "IMPULS", "IMPULSE"]
        "SHIELDS"   | "SH"         | ["SH", "SHI", "SHIE", "SHIEL", "SHIELD", "SHIELDS"]
        "PHASERS"   | "P"          | ["P", "PH", "PHA", "PHAS", "PHASE", "PHASER", "PHASERS"]
        "CLOAK"     | "CLOAK"      | ["CLOAK"]
        "CAPTURE"   | "CA"         | ["CA", "CAP", "CAPT", "CAPTU", "CAPTUR", "CAPTURE"]
        "REPORT"    | "REP"        | ["REP", "REPO", "REPOR", "REPORT"]
        "COMPUTER"  | "CO"         | ["CO", "COM", "COMP", "COMPU", "COMPUT", "COMPUTE", "COMPUTER"]
        "DOCK"      | "DO"         | ["DO", "DOC", "DOCK"]
        "REST"      | "R"          | ["R", "RE", "RES", "REST"]
        "MAYDAY"    | "MAYDAY"     | ["MAYDAY"]
        "ABANDON"   | "ABANDON"    | ["ABANDON"]
        "DESTRUCT"  | "DESTRUCT"   | ["DESTRUCT"]
        "QUIT"      | "QUIT"       | ["QUIT"]
        "SENSORS"   | "SE"         | ["SE", "SEN", "SENS", "SENSO", "SENSOR", "SENSORS"]
        "ORBIT"     | "O"          | ["O", "OR", "ORB", "ORBI", "ORBIT"]
        "TRANSPORT" | "T"          | ["T", "TR", "TRA", "TRAN", "TRANS", "TRANSP", "TRANSPO", "TRANSPOR", "TRANSPORT"]
        "SHUTTLE"   | "SHU"        | ["SHU", "SHUT", "SHUTT", "SHUTTL", "SHUTTLE"]
        "MINE"      | "MI"         | ["MI", "MIN", "MINE"]
        "CRYSTALS"  | "CR"         | ["CR", "CRY", "CRYS", "CRYST", "CRYSTA", "CRYSTAL", "CRYSTALS"]
        "PLANETS"   | "PL"         | ["PL", "PLA", "PLAN", "PLANE", "PLANET", "PLANETS"]
        "FREEZE"    | "FREEZE"     | ["FREEZE"]
        "REQUEST"   | "REQ"        | ["REQ", "REQU", "REQUE", "REQUES", "REQUEST"]
        "DEATHRAY"  | "DEATHRAY"   | ["DEATHRAY"]
        "PROBE"     | "PR"         | ["PR", "PRO", "PROB", "PROBE"]
        "EMEXIT"    | "E"          | ["E", "EM", "EME", "EMEX", "EMEXI", "EMEXIT"]
        "HELP"      | "HELP"       | ["HELP"]
        "SOME"      | ""           | ["SOME"]
        "SOMETHING" | null         | []

    }
}
