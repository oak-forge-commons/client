package pl.oakfusion.client.factory

import pl.oakfusion.client.data.Tree
import spock.lang.Specification

class OptionsTreeBuilderSpockTest extends Specification {

    def "should build tree with correct root"() {
        given:
        def treeBuilder = new OptionsTreeBuilder()

        when:
        def optionsTree = treeBuilder.buildTree(mnemonic, options)

        then:
        optionsTree.getRoot().getData() == mnemonic
        optionsTree.getRoot().getChildren().size() == numberOfChildren

        where:
        mnemonic  | options                                                                                      | numberOfChildren
        "STATUS"  | []                                                                                           | 0
        "SHIELDS" | [["UP"], ["DOWN"], ["TRANSFER"]]                                                             | 3
        "TORPEDO" | [["1", "2"], ["1", "4"], ["1", "6"]]                                                         | 1
        "REST"    | [["1"]]                                                                                      | 1
        "PROBE"   | [["MANUAL", "1"], ["AUTOMATIC", "2"], ["ARMED", "MANUAL", "1"], ["ARMED", "AUTOMATIC", "2"]] | 3
    }

    def "should build correct tree"() {
        given:
        def treeBuilder = new OptionsTreeBuilder()
        def mnemonic = "MOVE"
        def options = [["MANUAL", "1"], ["MANUAL", "2"], ["AUTOMATIC", "2"], ["AUTOMATIC", "4"]]

        when:
        def optionsTree = treeBuilder.buildTree(mnemonic, options)

        then:
        optionsTree.getRoot().getData() == mnemonic
        optionsTree.getRoot().getChildren().size() == 2
        optionsTree.getRoot().getChildren().contains(new Tree.Node<String>("MANUAL"))
        optionsTree.getRoot().getChildren().contains(new Tree.Node<String>("AUTOMATIC"))
        for (Tree.Node<String> child : optionsTree.getRoot().getChildren()) {
            child.getChildren().size() == 2
        }
    }
}
