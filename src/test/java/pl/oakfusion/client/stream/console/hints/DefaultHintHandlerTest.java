package pl.oakfusion.client.stream.console.hints;

import org.junit.jupiter.api.Test;
import pl.oakfusion.client.Writer;
import pl.oakfusion.client.data.MessageConfigurationMapping;
import pl.oakfusion.client.data.UserInput;
import pl.oakfusion.client.stream.StreamWriter;
import pl.oakfusion.data.message.Event;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultHintHandlerTest {
    private final MessageConfigurationMapping messageConfigurationMapping = new MessageConfigurationMapping();
    private final UserInput userInput = new UserInput();


//    @Test
//    void should_show_hint_for_move_command() throws IOException {
//        //given..
//
//        messageConfigurationMapping.parseCommands();
//        HintHandler hintHandler = new DefaultHintHandler(messageConfigurationMapping.getCommandsMap(),
//                messageConfigurationMapping.getOptionTreesMap());
//
//        userInput.setMnemonic("TORPEDO");
//
//        //when..
//        Event hintEvent = hintHandler.handle(userInput);
//
//        //then..
//        assertThat(hintEvent).isInstanceOf(HintEvent.class);
//    }


//    @Test
//    void should_show_hint_for_move_command() throws IOException {
//        //given..
//        messageConfigurationMapping.parseCommands();
//        HintHandler hintHandler = new DefaultHintHandler(writer, messageConfigurationMapping.getCommandsMap(),
//                messageConfigurationMapping.getCommandsOptions(), messageConfigurationMapping.getOptionTreesMap());
//
//        userInput.setMnemonic("TORPEDO");
//
//        Logger logger = (Logger) LoggerFactory.getLogger(DefaultHintHandler.class);
//
//        ListAppender<ILoggingEvent> listAppender = new ListAppender();
//        listAppender.start();
//
//        logger.addAppender(listAppender);
//
//        //when..
//        hintHandler.handle(userInput);
//
//        //then..
//        assertThat(listAppender.list).extracting(ILoggingEvent::getMessage).contains("Command incorrect. You can try one of the following combinations:");
//    }

}
